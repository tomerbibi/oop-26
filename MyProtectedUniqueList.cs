﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___26
{
     public class MyProtectedUniqueList
    {
        private List<string> _words = new List<string>();
        private string _key;

        public MyProtectedUniqueList(string key)
        {
            _key = key;
        }
        public void Add(string newWord)
        {
            if (newWord == null)
                throw new ArgumentNullException("you sent a null string");

            else if (_words.Contains(newWord))
                throw new InvalidOperationException($"the word {newWord} is exist in the list");

            else
                _words.Add(newWord);
        }

        public void Remove(string Word)
        {
            if (Word == null)
                throw new ArgumentNullException("you sent a null string");

            else if (!_words.Contains(Word))
                throw new InvalidOperationException($"the word {Word} doesnt exist in the list");

            else
                _words.Remove(Word);
        }
        public void RemoveAt(int index)
        {
            if (_words.Count > 0 && index > 0 && index <= _words.Count)
                _words.RemoveAt(index);

            else
                throw new ArgumentOutOfRangeException($"the index {index} doesnt exist in the list");
        }
        public void Clear(string key)
        {
            if (key == _key)
                _words.Clear();

            else
                throw new AccessViolationException($"{key} does not match the key word");
        }
        public void Sort(string key)
        {
            if (key == _key)
                _words.Sort();

            else
                throw new AccessViolationException($"{key} does not match the key word");
        }

        public override string ToString()
        {
            string s = "";
            _words.ForEach(item => s = s + $"{item}, ");
            return s;
        }
    }
}
