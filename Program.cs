﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___26
{
    class Program
    {
        static void Main(string[] args)
        {
            MyProtectedUniqueList m = new MyProtectedUniqueList("key");
            m.Add("apple");
            try
            {
                m.Add(null);
            }
            catch (ArgumentNullException a)
            {

            }
            try
            {
                m.Add("apple");
            }
            catch(InvalidOperationException i)
            {

            }
            try
            {
                m.Remove(null);
            }
            catch (ArgumentNullException a)
            {

            }
            try
            {
                m.Remove("banana");
            }
            catch (InvalidOperationException i)
            {

            }
            try
            {
                m.RemoveAt(999);
            }
            catch(ArgumentOutOfRangeException a)
            {

            }
            try
            {
                m.Clear("hhh");
            }
            catch(AccessViolationException)
            {

            }
            try
            {
                m.Sort("hhh");
            }
            catch (AccessViolationException)
            {

            }
        }
    }
}
